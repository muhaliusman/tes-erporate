<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Guarded variable
     *
     * @var array
     */
    protected $guarded = ['updated_at'];

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $code = 'ERP';
            $date = date('dmY');

            $dataInMonth = self::where('transaction_code', 'like', '%'. date('mY') .'%')
                ->orderBy('transaction_code', 'desc')
                ->first();

            $number = '001';

            if ($dataInMonth) {
                $no = (int) explode('-', $dataInMonth->transaction_code)[1] + 1;
                $number = sprintf("%03s", $no);
            }

            $transactionCode = $code . $date . '-' . $number;
            $model->transaction_code = $transactionCode;
            $model->user_id = auth()->user()->id;
        });
    }

    /**
     * Relation menu
     *
     * @return Eloquent
     */
    public function menus()
    {
        return $this->belongsToMany('App\Menu');
    }
}
