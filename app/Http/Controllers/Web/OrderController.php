<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Menu;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::with('menus');

        $user = auth()->user();

        //if pelayan
        if ($user->role === '2') {
            $order = $order->where('user_id', $user->id);
        }

        $order = $order->get();

        return view('order')->withOrders($order);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::where('status', 'AVAILABLE')->get();
        return view('order-form')->withMenus($menus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order = Order::create([
            'table_number' => $request->table_number
        ]);

        $order->menus()->attach($request->menus);

        return redirect()->route('orders.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = Menu::where('status', 'AVAILABLE')->get();
        $order = Order::findOrFail($id);
        $menus = $menus->merge($order->menus);

        return view('order-form')->with([
            "menus" => $menus,
            "order" => $order,
            "edit" => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'table_number' => $request->table_number
        ]);

        $order->menus()->sync($request->menus);

        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->role === '1') {
            abort(403);
        }

        Order::findOrFail($id)->delete();

        if (request()->ajax()) {
            return response()->json([
                'success' => true
            ], 200);
        }

        return redirect()->route('orders.index');
    }

    /**
     * Close transaction
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function closeTransaction($id)
    {
        if (!auth()->user()->role === '1') {
            abort(403);
        }

        Order::findOrFail($id)->update([
            'status' => 'CLOSE'
        ]);

        if (request()->ajax()) {
            return response()->json([
                'success' => true
            ], 200);
        }

        return redirect()->route('orders.index');
    }
}
