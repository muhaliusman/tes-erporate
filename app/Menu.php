<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * Guarded
     *
     * @var array
     */
    protected $guarded = ['updated_at'];

    /**
     * Relation menu
     *
     * @return Eloquent
     */
    public function orders()
    {
        return $this->belongsToMany('App\Order');
    }
}
