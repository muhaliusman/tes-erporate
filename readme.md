# Tes Erporate

Tes Erporate

## Getting Started

This project uses laravel 5.7

### Installing


Very easy to install

```
git clone https://gitlab.com/muhaliusman/tes-erporate.git
```


Copy .env.example to .env

```
cp .env.example .env
```


Setup .env file

```
...

APP_URL=http://localhost

...

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_database_user
DB_PASSWORD=your_database_password

```


Install dependencies

```
composer install
```


Generate app key

```
php artisan key:generate
```


Run migrate and seed

```
php artisan migrate --seed
```


Run the project

```
php artisan serve
```


Credential

```
KASIR
username : kasir@gmail.com
password : 12345

PELAYAN 1
username : pelayan1@gmail.com
password : 12345

PELAYAN 2
username : pelayan2@gmail.com
password : 12345
```


API Documentation

```
http://[your_base_url]/api/documentation

ex: http://localhost:8000/api/documentation
```

Thank you very much