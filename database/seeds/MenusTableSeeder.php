<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            ['title' => 'Kepiting Saus Tiram', 'status' => 'AVAILABLE'],
            ['title' => 'Udang Asam Manis', 'status' => 'NOT_AVAILABLE'],
        ];
        DB::table('menus')->insert($menus);
    }
}
