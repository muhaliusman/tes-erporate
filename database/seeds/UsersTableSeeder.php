<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'Kasir', 'email' => 'kasir@gmail.com', 'password' => bcrypt('12345'), 'role' => '1', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Pelayan1', 'email' => 'pelayan1@gmail.com', 'password' => bcrypt('12345'), 'role' => '2', 'created_at' => date('Y-m-d H:i:s')],
            ['name' => 'Pelayan2', 'email' => 'pelayan2@gmail.com', 'password' => bcrypt('12345'), 'role' => '2', 'created_at' => date('Y-m-d H:i:s')],
        ];
        DB::table('users')->insert($users);
    }
}
