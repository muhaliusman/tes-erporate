@extends('layouts.app')

@section('content')
@php
    (empty($edit) ? $edit = false : $edit = true)
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">@if($edit) Ubah @else Tambah @endif Menu</div>
                <div class="card-body">
                    <form role="form" method="POST" action="@if($edit) {{ route('menus.update', ['menu' => $menu->id]) }} @else {{ route('menus.store') }} @endif">
                        @csrf

                        @if ($edit)
                        {{ method_field('PUT') }}
                        @endif
                        <div class="form-group">
                            <label class="control-label" for="title">NAMA MENU</label>
                            <input class="form-control" name="title" id="title" value="@if($edit) {{ $menu->title }} @else {{ old('title') }} @endif">
                            @if ($errors->any())
                            @if(isset($errors->messages()['title']))
                            <small class="form-text text-danger">
                                {{ implode(', ', $errors->messages()['title']) }}
                            </small>
                            @endif
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label">STATUS</label>
                            <div class="radio">
                                <label><input type="radio" name="status" value="AVAILABLE" @if($edit) @if($menu->status === 'AVAILABLE') checked @endif @else checked @endif> AVAILABLE</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="status" @if($edit) @if($menu->status === 'NOT_AVAILABLE') checked @endif @endif value="NOT_AVAILABLE"> NOT AVAILABLE</label>
                            </div>
                            @if ($errors->any())
                            @if(isset($errors->messages()['status']))
                            <small class="form-text text-danger">
                                {{ implode(', ', $errors->messages()['status']) }}
                            </small>
                            @endif
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

