@extends('layouts.app')

@section('content')
@php
    (empty($edit) ? $edit = false : $edit = true)
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">@if($edit) Ubah @else Tambah @endif Order</div>
                <div class="card-body">
                    @if($edit)
                    <p>Nomer Transaksi : <b>{{ $order->transaction_code }}</b></p>
                    @endif
                    <form role="form" method="POST" action="@if($edit) {{ route('orders.update', ['order' => $order->id]) }} @else {{ route('orders.store') }} @endif">
                        @csrf

                        @if ($edit)
                        {{ method_field('PUT') }}
                        @endif
                        <div class="form-group">
                            <label class="control-label" for="table_number">Nomer Meja</label>
                            <input class="form-control" name="table_number" id="table_number" value="@if($edit) {{ $order->table_number }} @else {{ old('title') }} @endif">
                            @if ($errors->any())
                            @if(isset($errors->messages()['table_number']))
                            <small class="form-text text-danger">
                                {{ implode(', ', $errors->messages()['table_number']) }}
                            </small>
                            @endif
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label">Menu</label>
                            @foreach ($menus as $item)
                            <div class="checkbox">
                                <label><input name="menus[]" type="checkbox" value="{{ $item->id }}" @if($edit) @if($order->menus->contains('id', $item->id)) checked @endif @endif> {{ $item->title }}</label>
                            </div>
                            @endforeach
                            @if ($errors->any())
                            @if(isset($errors->messages()['menus']))
                            <small class="form-text text-danger">
                                {{ implode(', ', $errors->messages()['menus']) }}
                            </small>
                            @endif
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">SIMPAN</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

