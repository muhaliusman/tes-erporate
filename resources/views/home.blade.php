@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3 class="text-center">Halloo {{ auth()->user()->name }}</h3>
                    <hr>
                    <div class="text-center">
                        <a href="{{ route('menus.index') }}" class="btn btn-primary">Lihat Menu</a>
                        <a href="" class="btn btn-success">Buat Pesanan</a>
                        <a href="{{ route('orders.index') }}" class="btn btn-warning">Lihat Pesanan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
