@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Daftar Menu</div>
                <div class="card-body">
                    @if (auth()->user()->role === '1')
                    <div class="text-right">
                        <a href="{{ route('menus.create') }}" class="btn btn-primary">Tambah Menu</a>
                        <br><br>
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    @if (auth()->user()->role === '1')
                                    <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($menus as $index => $item)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ str_replace_first('_', ' ', $item->status) }}</td>
                                    @if (auth()->user()->role === '1')
                                    <td>
                                        <a href="{{ route('menus.edit', ['menu' => $item->id]) }}" class="btn btn-sm btn-warning">Ubah</a>
                                        <a href="{{ route('menus.destroy', ['menu' => $item->id]) }}" class="btn-delete btn btn-sm btn-danger">Hapus</a>
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>
<script>
    $(function(){
        $('.btn-delete').on('click', function(e) {
            e.preventDefault();
            const delUrl = $(this).attr('href');
            Swal({
                title: 'Konfirmasi Hapus Data',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                showLoaderOnConfirm: true,
                preConfirm: (login) => {
                    return fetch(delUrl, {
                            method:"DELETE",
                            headers: {
                                'X-CSRF-TOKEN': "{{ csrf_token() }}",
                            },
                        })
                        .then(response => {
                            if (response.status != 200) {
                                Swal({
                                    type : 'error',
                                    title : 'Error',
                                    text : 'Ups, Terjadi Kesalahan',
                                    confirmButtonText: 'OK',
                                    preConfirm: () => location.reload()
                                });
                            }
                        })
                        .catch(() => {
                            Swal({
                                type : 'error',
                                title : 'Error',
                                text : 'Ups, Terjadi Kesalahan',
                                confirmButtonText: 'OK',
                                preConfirm: () => location.reload()
                            });
                        })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result) {
                    Swal({
                        type : 'success',
                        title : 'Success',
                        text : 'Data Berhasil Dihapus',
                        confirmButtonText: 'OK',
                        preConfirm: () => location.reload()
                    });
                }
            })
        });
    });
</script>
@endsection
