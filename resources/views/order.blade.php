@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Daftar Menu</div>
                <div class="card-body">
                    <div class="text-right">
                        <a href="{{ route('orders.create') }}" class="btn btn-primary">Tambah Menu</a>
                        <br><br>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <th>#</th>
                                <th>Nomer Order</th>
                                <th>Nomer Meja</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($orders as $index => $order)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td data-toggle="collapse" data-target="#accordion{{ $index }}" class="clickable" style="cursor:pointer; color:blue">{{ $order->transaction_code }}</td>
                                    <td>{{ $order->table_number }}</td>
                                    <td>{{ $order->status }}</td>
                                    <td>
                                        <a href="{{ route('orders.edit', ['order' => $order->id]) }}" class="btn btn-sm btn-warning">Edit</a>
                                        @if($order->status !== 'CLOSE' && auth()->user()->role === '1')
                                        <a href="{{ route('orders.close', ['order' => $order->id]) }}" class="btn btn-sm btn-primary btn-close" >Bayar</a>
                                        @endif
                                        @if(auth()->user()->role === '1')
                                        <a href="{{ route('orders.destroy', ['order' => $order->id]) }}" class="btn btn-sm btn-danger btn-delete">Hapus</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="padding:0px;">
                                        <div id="accordion{{ $index }}" class="collapse" style="padding:10px;">
                                            <label>Menu: </label>
                                            <ol>
                                                @foreach ($order->menus as $item)
                                                <li>{{ $item->title }}</li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.32.2/dist/sweetalert2.all.min.js"></script>
<script>
    function swalInit(url, methodReq, title, buttonText, successMessage) {
        Swal({
            title: title,
            showCancelButton: true,
            confirmButtonText: buttonText,
            showLoaderOnConfirm: true,
            preConfirm: (login) => {
                return fetch(url, {
                        method: methodReq,
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}",
                        },
                    })
                    .then(response => {
                        if (response.status != 200) {
                            Swal({
                                type : 'error',
                                title : 'Error',
                                text : 'Ups, Terjadi Kesalahan',
                                confirmButtonText: 'OK',
                                preConfirm: () => location.reload()
                            });
                        }
                    })
                    .catch(() => {
                        Swal({
                            type : 'error',
                            title : 'Error',
                            text : 'Ups, Terjadi Kesalahan',
                            confirmButtonText: 'OK',
                            preConfirm: () => location.reload()
                        });
                    })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result) {
                Swal({
                    type : 'success',
                    title : 'Success',
                    text : successMessage,
                    confirmButtonText: 'OK',
                    preConfirm: () => location.reload()
                });
            }
        })
    }

    $(function(){
        $('.btn-delete').on('click', function(e) {
            e.preventDefault();
            const delUrl = $(this).attr('href');
            swalInit(delUrl, 'DELETE', 'Hapus Data', 'Hapus', 'Data Berhasil Dihapus');
        });

        $('.btn-close').on('click', function(e) {
            e.preventDefault();
            const delUrl = $(this).attr('href');
            swalInit(delUrl, 'PUT', 'Bayar Dan Tutup Order', 'Bayar', 'Data Terbayar');
        });
    });
</script>
@endsection
