<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('menus', 'MenuController')->except(['show']);
    Route::put('orders/close-transaction/{order}', 'OrderController@closeTransaction')->name('orders.close');
    Route::resource('orders', 'OrderController')->except(['show']);
});
